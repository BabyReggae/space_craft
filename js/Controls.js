window.addEventListener('resize', function () {
    camera.aspect = window.innerWidth / window.innerHeight;
    camera.updateProjectionMatrix();
    renderer.setSize(window.innerWidth, window.innerHeight)
})

document.addEventListener("keydown", function(event){
    var Button = event.which
    if(Button == 37 || Button == 81){
        // Q or leftArrow Clicked
        ActiveKeys.left = true        
    }else
    if(Button == 38 || Button == 90){
        // Z or UpArrow Clicked
        ActiveKeys.up = true
    }else
    if(Button == 39 || Button == 68){
        // D or RightArrow Clicked
        ActiveKeys.right = true
    }else
    if(Button == 40 || Button == 83){
        // S or DownArrow Clicked
        ActiveKeys.down = true
    }else
    if(Button == 32){
        // Space Clicked
        if(Player != null || Player != undefined) Player.Shoot()
    }else
    if(Button == 80){
        // P Clicked
        if(Player != null || Player != undefined) Player.POV()
    }
});

window.addEventListener('click', function () {
    ActiveKeys.shoot = true
    setTimeout(function(){ActiveKeys.shoot = false},10)
    
})

document.addEventListener("keyup", function(event){
    var Button = event.which
    if(Button == 37 || Button == 81){
        // Q or leftArrow Clicked
        ActiveKeys.left = false   
        if(Player != null || Player != undefined) Player.Mesh.rotation.z = -3.14     
    }else
    if(Button == 38 || Button == 90){
        // Z or UpArrow Clicked
        ActiveKeys.up = false
    }else
    if(Button == 39 || Button == 68){
        // D or RightArrow Clicked
        ActiveKeys.right = false
        if(Player != null || Player != undefined) Player.Mesh.rotation.z = -3.14     
    }else
    if(Button == 40 || Button == 83){
        // S or DownArrow Clicked
        ActiveKeys.down = false
    }
});
