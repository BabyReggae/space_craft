var loader = new THREE.ObjectLoader()

var sceneScript = document.createElement("script")
sceneScript.setAttribute("src", "./js/Game.js")




var ObjectType = {
    Player: 0,
    Enm_1: 1,
    Enm_2: 2,
    Rocket: 3,
    Sheild: 4,
    Coin: 5
}

var ProtoTypes = [
    { url: "./3Dobjets/Player.json", Mesh: null },
    { url: "./3Dobjets/Enm_1.json", Mesh: null },
    { url: "./3Dobjets/Enm_2.json", Mesh: null },
    { url: "./3Dobjets/Rocket.json", Mesh: null },
    { url: "./3Dobjets/Sheild.json", Mesh: null },
    { url: "./3Dobjets/Coin.json", Mesh: null }
]

function LoadProtoTypes() {
    var counter = 0
    ProtoTypes.forEach(elm => {
        loader.load(

            elm.url,

            function (obj) {
                elm.Mesh = obj;
                counter += 1
                console.log(elm.url, "Was loaded")
                if (counter == ProtoTypes.length) document.head.appendChild(sceneScript)
            },

            function (xhr) {
                //console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
            },

            function (err) {
                console.error('An error happened while loading the 3D object');
            }
        )
    })
}

LoadProtoTypes()